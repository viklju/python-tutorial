# Python Tutorial
This is a Python tutorial I started writing to consolodate my knowledge, with the idea of having a single place of reference.
Though I quickly got inspired by https://learnxinyminutes.com/docs/python/, which I though lacked a lot of detail.

## Use
This was used to help teachers learn Python, given that they didn't use it for very long it wasn't a huge success.
But a couple of friends used this guide for their university stuides where it seems to have been quite usefull

## Overview
intro: DONE
 -  0. Basics
 -  1. Numbers
 -  2. Arithmetic Operators
 -  3. Variables
 -  4. More Operators
 -  5. Comparison Logic
 -  6. Some Useful Functions
 -  7. Combining What We Know

syntax deep dive: DONE
 -  S0. Values
 -  S1. Operators
 -  S2. Variables
 -  S3. Functions
 -  S4. Keywords
 -  Tasks
 -  Glossary

loops and functions: FUNCTIONS AND PYTHON TACTICS NOT DONE
 -  8. Loops
 -  9. Inputs
 -  10. Errors
 -  11. Functions
 -  12. Python Tactics

lists: DONE
 -  13. Lists
 -  14. More Indexing
 -  15. Lists and Functions
 -  16. Tuples
 -  17. Dictionaries
 -  18. Structuring Data

string magic: DONE
 -  19. String Methods
 -  20. String Formatting
 -  21. Print and String Formatting

math: NOT DONE
 -  M1. Random (Even or odd, calcualte which number)
 -  M2. Math Module
 -  M3. Graphs (y = kx + m)
