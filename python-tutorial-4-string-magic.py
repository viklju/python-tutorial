#!/usr/bin/env python3
###############################################################################
# 19. String Methods
###############################################################################

# String Operations
"hello world".upper()       # => "HELLO WORLD"
"HELLO WORLD".lower()       # => "hello world"
"hello WORLD".capitalize()  # => "Hello world"
"HELLO world".title()       # => "Hello World"
"HELLO world".swapcase()    # => "hello WORLD"

"hello world".islower()            # => True
"hello world".isupper()            # => False
"Hello World".istitle()            # => True
"hello world".endswith("world")    # => True
"hello world".startswith("world")  # => False

"12345".isalpha()          # => False
"hello world".isalpha()    # => True
"12345".isnumeric()        # => True
"hello world".isnumeric()  # => False
"hello! 12345".isalnum()   # => False
"hello 12345".isalnum()    # => True


# Space padding strings
"hello world".center(22)       # => '     hello world      '
"hello world".ljust(22)        # => 'hello world           '
"hello world".rjust(22)        # => '           hello world'
'     hello world  '.strip()   # => 'hello world'
'     hello world  '.rstrip()  # => '     hello world'
'     hello world  '.lstrip()  # => 'hello world  '

# More string operators
"Hi world. Hi!".replace("Hi", "hello")     # => 'hello world. hello!'
"hi world. Hi!".replace("Hi", "hello")     # => 'hi world. hello!'
"Hi world. Hi!".replace("Hi", "hello", 1)  # => 'hello world. Hi!'
"Hi world. Hi!".replace("world.", "")      # => 'hi  Hi!'

"hello world".count("l")  # => 3
# counts the occurences of the sub string 'l' in the string "hello world"


###############################################################################
# 20. Do More with Strings
###############################################################################

# Arithmetic operators and strings

# As you already know you can combine strings
"Hello" + "World"  # => "HelloWorld"

# Multiplication can be done using integers, i.e.:
"hello " * 4  # => "hello hello hello hello"
# As far as I know these are the valid operators when using strings


# Escape characters
"you can use: ' whitin a string"
'you can use: " whitin a string'
"# be carefull with mixing"
"or you could escape \"'\", to use both"

"you can even escape \\ the escape"

"if you want a single line written on\
multiple lines of code, escape!"

"start on one line\nand continue on another"
"create tabs with\t"


# Print more stuff
print("hi", "hello", "are you there?")  # => "hi hello are you there?"
print(2, 3.9, "all the numbers")        # => "2 3.9 all the numbers"
# When possible this is better than converting to strings for printing

print("hi", "hello", "are you there?", sep='')  # => "hihelloare you there?"

print("Hello", end="!")
print(" How are you?")   # => "Hello! How are you?"
print("I'm good\nHow are you?")
# =>
# I'm good
# How are you?

# Strings can be formatted
"{} can be {}".format("Strings", "interpolated")
# => "Strings can be interpolated"

# You can repeat the formatting arguments to save some typing
"{0} be nimble, {0} be quick, {0} jump over the {1}".format("Jack", "candle stick")
# => "Jack be nimble, Jack be quick, Jack jump over the candle stick"

# You can use keywords if you don't want to count.
"{name} wants to eat {food}".format(name="Bob", food="lasagna")
# => "Bob wants to eat lasagna"

# f-strings or formatted strings, are very powerfull
name = "John"
f"{name} is {len(name)} characters long."  # => 'John is 4 characters long.'
# Note: how it can utilize variables set outside of the string declaration

# Of course something similar can be done with string formatting
name = "John"
"{} is {} characters long".format(name, len(name))
# => 'John is 4 characters long'

# Dictionaries and formatted strings
dict = {'first': 1, 'second': 2}
"{p[first]} and {p[second]}".format(p=dict)  # => '1 and 2'
"{first} and {second}".format(**dict)        # => '1 and 2'


###############################################################################
# 21. Print and String Formatting
###############################################################################

# Expanding on string formatting

# Floats and string formatting
print("integer: {}, float: {:f}".format(75, 85))
# Note: floats/integers are turned into strings when using string formatting

# Change the accuracy of numbers
print("less floaty {0:.1f}, more floaty {1:.3f}, not floating {1:.0f}".format(74, 12.76428))

# Zero padd
print("{:06.2f}".format(3.14159))  # => '003.14'
# .2f trunctates to '3.14' and 06 zero padd to six characters (including '.')

# Truncate long strings
print("{:.5}".format('xylophone'))  # => 'xylop'

# Prefix numbers
print("Positive: {:+}, Negative: {:+}".format(12, -3.2))
# By default only negative numbers have leading negative symbol
print("Positive: {: +}, Negative: {: +}".format(12, -3.2))

# Space padding strings
print("{:4} {:16}".format(2, 34))
# This inserts 4 spaces before '2', and 16 spaces before '34'

print("{:>4} {:>16}".format(2, 34))  # Left-aliging: before inserting (default)
print("{:<4} {:<16}".format(2, 34))  # Right-aliging: after inserting
print("{:^4} {:^16}".format(2, 34))  # Center-aliging: insert in the middle
# Note: center-aliging will insert one space then 4 and after it two spaces
# i.e. ' 4  ', the inserted number counts towards the spaces

# Dynamic aligning
print('{:{align}{width}}'.format('test', align='^', width='10'))


# Choose your padding
print("{:*<10}{:*<10}!".format("hello", "world"))
print("{:o<10}{:d<10}!".format("hello", "world"))
# Note: must specify aliging
# Specify any single character, note: don't quote the characters

# Combinging what we know - print a matrix
print('-' * 34)
print('| {:^8} | {:^8} | {:^8} |'.format('i ** 1', 'i ** 2', 'i ** 3'))
print('+', '-' * 8, '+', '-' * 8, '+', '-' * 8, '+')
for i in range(3, 40, 3):
    print('| {:^8} | {:^8} | {:^8} |'.format(i, i * i, i ** 3))
    # print('|    {!s:<6}|    {!s:<6}|    {!s:<6}|'.format(i, i * i, i ** 3))
print('+', '-' * 8, '+', '-' * 8, '+', '-' * 8, '+')

# Result:
"""
----------------------------------
|  i ** 1  |  i ** 2  |  i ** 3  |
+ -------- + -------- + -------- +
|    3     |    9     |    27    |
|    6     |    36    |   216    |
|    9     |    81    |   729    |
|    12    |   144    |   1728   |
|    15    |   225    |   3375   |
|    18    |   324    |   5832   |
|    21    |   441    |   9261   |
|    24    |   576    |  13824   |
|    27    |   729    |  19683   |
|    30    |   900    |  27000   |
|    33    |   1089   |  35937   |
|    36    |   1296   |  46656   |
|    39    |   1521   |  59319   |
+ -------- + -------- + -------- +
"""
# There is a lot of things happening in the matrix code.
# To better understand it, try changing it too see what happens.
# The commented line prints the same matrix, just left adjusted,
# instead of centered
