#!/usr/bin/env python3
###############################################################################

# This is a somewhat simplified look at the Python syntax, with some
# further examples and explanations. It is intended as a compliment to
# 'python tutorial 1 intro'

# The numbered Python tutorials covers a more comprehensively what you can do
# in Python

# This gudie assumes you've had difficulties understanding how to apply, or
# write your own code after reading (portions of) 'python tutorial 1 intro'


###############################################################################
# S0. Values
###############################################################################

# Further explaination of 0. Basics and 1. Numbers
# Integers are simply numbers
5  # 5 as an integer

# Floats are decimal numbers (Note: '.' not ',')
5.0                # 5 as a float
3.141592653589793  # pi as a float

# Strings are letters, or numbers as a word
'a bunch of letters forming a string of characters'
"pi: 3.141592653589793, of course this isn't the entirety of pi"

# Strings can be any character that starts and stops with " or '

# Booleans are binary yes or no, in Python there is no maybe
True
False
# Capitalization is very important in Python
# Everything in Python is case-sensative


###############################################################################
# S1. Operators
###############################################################################

# Further examples for 2. Arithmetic Operators
# General syntax: <value> <operator> <value>
# A line of code continaing an operator is called an expression in Python
3 * 5  # => 15

# an operator will return a new value

# Operators will work with integers and floats. '+'-operator support strings
# and '*'-operator can mix both strings and integers
"string" + 'second string'  # => 'stringsecond string'
3 * "string"                # => 'stringstringstring'
# Note: when mixing strings and integers, the integers must be first.
# Since you combine the "string" 3-times, not combine 3 "string"-times.

# Booleans are generally not used with operators, though they are returned when
# using comparison operators, such as:
3 > 10  # => True
2 == 3  # => False


###############################################################################
# S2. Variables
###############################################################################

# Variables are assigned a value, by using the operator '='
# General syntax: <variable> = <value>
variable = "hi"

# Variables are names that start with a letter.
# Though it is allowed in Python, please avoid naming your variables the
# same name as an exisiting function, as that will overwrite the function.

# Variables can be used in Python instead of the acctual value it represents,
# for example when using operators:
3 * variable  # => 'hihihi'


###############################################################################
# S3. Functions
###############################################################################

# Functions are callable pieces of code, which can take one or more values
# (depending on the function), and does something with the value(s)

# Functions can be identified by '()' after their name, which in Python means
# asking the computer to call on the function with the name before '()',
# i.e. example()

# For example one of the most common functions; the print()-function, which
# takes a value and displays it:
print("hello world!")  # displays the string "hello world!"

# Some functions return a new value, e.g.:
str(5)  # => '5'
# the str()-function, which takes a float or integer and return that value as
# a string

# Other can take multiple values:
min(1, 2, 3)  # => 1 (returns the smallest value)

# As previouesly mentioned, you could name a variable print, but then
# attempts at using the function print() would instead call on your variable
# which isn't a function, thereby causing an error


###############################################################################
# S4. Keywords
###############################################################################

# Certain words are used in statements in Python, and can be divided into two
# general categories:

# 1. evalution
# General syntax: <value> <evaluation> <value>

True is False   # => False (Truthness-evalution)
"h" in "hello"  # => True
True and False  # => False
False or True   # => True
not False       # => True

# 2. conditional - do something if certain criteria are met
# General syntax:
# <conditional> <expression>:
#     <code-block>

# Note: mind the indent for the code-block (4 spaces/tab of equal length)
# All conditional statement must be followed by one code-block

if 2 > 3:
    print("this")

# the if-statemnt evalutes a boolean, or something that can return a boolean.
# e.g. boolean-operators or evaluation-statements.
# The indented code run only if the
# expression (boolean/boolean-operator/evaluation-statement) is True

# The else-statement must be preceeded by a if statement, and is True only when
# the preceeding if-statement is False, i.e.:
if False:
    not True
else:
    True

# The elif-statement is a combination of the if and else statements.
# And is only executed if the preceeding if-statement is False, and if the
# elif-statement is True.
if False:
    not True
elif False:
    not True
elif True:
    True
else:
    False


###############################################################################
# Tasks
###############################################################################

# Take care in testing your code, for the tasks below. Make sure to try many
# different values.

# 1. double
# write a piece of code which takes a variable, containging a number, and
# doubles the value of the variable. Use the print to display the result.

# 2. compare
# Write a piece of code which takes a variable and prints "even!" if the
# variable contains an even number and "odd!", if it contains an odd number.

# 3. more if
# Use your solution from 2. compare, and now if the number is even check if the
# number is dividable by 4, and if it is - print "double even!".
# If the number is odd check if the number is dividable by 6, and if it is
# - print "double uneven!"


###############################################################################
# Glossary
###############################################################################

"""
VALUES: - can be one of
strings: str
integers: int
floats: float
booleans: bool

ARITHMETIC OPERATORS: - returns an integer or float
addition: +
subtraction: -
mutiplication: *
division: /
floor division: //
modulo operation: %
Exponentation: **

COMPARISION OPERATORS: - compares values, and returns a boolean
less: <
more: >
less or equal: =<
more or equal: =>
is equal: ==
is inequal: !=

EVALUATION STATEMENTS: - returns a boolean
compare truthness: is
logical and: and
logical or: or
portion in main: in
negate: not

CONDITIONAL STATEMENTS: - controls the flow of code
if expression:
    code-block
elif expression:
    code-block
else:
    code-block

BASIC FUNCTIONS:
display: print()

return value type: type()
check if value is type: isinstance()

return lenght of string: len()
return absolut value: abs()
return round value: round()
return min value: min()
return max value: max()

retrun string: str()
return integer: int()
retun float: float()
"""
