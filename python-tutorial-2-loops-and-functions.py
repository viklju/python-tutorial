#!/usr/bin/env python3
###############################################################################
# 8. Loops
###############################################################################

# Simple loops - While-loops
a = 4
while a > 1:
    a -= 1

# The while-statement is first True, since a is 4, which inturn is more than 1
# After one iteration a is now 3, which means the while-statement is still True
# So the code loops once more, until a is 1 or less, for a total of three loops
# See for your self by printing a (print(a)) whitin the while loop
# Try moving the print-statement before/after a -= 1, what changes?

# More while loops
while True:  # => while True is True loop, i.e. an endless loop
    if False:
        continue  # => if False is True continue the while-loop

    if True:
        break  # => if True is True exit the loop

# Assigning values, and save some typing
a, b, c = 1, 2, 3
# => a = 1
# => b = 2
# => c = 3

hello = "world"
world = "hello"
hello, world = world, hello
print(hello)

range(3)  # => 0, 1, 2
a, b, c = range(3)
# a, b, c = range(4)       # => 'ValueError: too many values to unpack'
# a, b, c = range(2)       # => 'ValueError: not enough values to unpack'
a, b, c = range(1, 4)      # => a = 1, b = 2, c = 3
a, b, c = range(0, 6, 2)   # => a = 0, b = 2, c = 4
# range(start, stop, step), by default start = 0 and step = 1

# Simple loops - For-loops
for i in 1, 2, 3:
    print(i)

# For-loops doens't check for Truthness, rather it loops a defined number of
# times. In this example it will loop three times, each iteration the variable
# i, will be assigned a new value. First 1, then 2 and lastly 3.
# The loop will execute any code within the indent each iteration.

# Note: just like with the while-loop, the variables in the for-loop will still
# be assigned the last value after the loop, i.e. try print(i) after the for-loop


# While-loops are often used when you need to do something until a condition is met
# For-loops are often used when you need to do something a specific number of times

# Though both loops can be used to solve the same task
# But depending on the task one might be simpler to work with, than the other


# Example 1 - calculate the factorial of n-th size
var = -5
sum = 1

for i in range(abs(var)):
    sum *= (i + 1)

print(sum)  # => 120


# Example 2 - calculate the factorial of n-th size
var = -5
sum = 1

var = abs(var)

while var != 0:
    sum *= var
    var -= 1

print(sum)  # => 120


###############################################################################
# 9. Inputs
###############################################################################

# Get input from user
input("hello world")  # => prints 'hello world' and waits for input
# All inputs are strings

integer = int(input("input integer: "))
# Pass input-functions output to int-function, to turn user input to an integer
# Note: if a user inputs a character which cannot be turned into an integer,
# the code will throw an error

###############################################################################
# 10. Errors
###############################################################################

# Errors or exceptions happen when Python can't understand your code
# They aren't dangerous, but it means that your code stops running.

# print(  # => SyntaxError: invalid syntax
# a + 7   # => NameError: name 'a' is not defined
# 10 / 0  # => ZeroDivisionError: division by zero

# This is normally not a problem, after all errors help you find what went wrong
# But some times they can get in the way...
# Let's say you have a variable which your not entirely sure of what it is,
# but still need to work with. For example:

number = input("pick a number: ")
print("this is half your number")
print(number / 2)  # what if the user doesn't input a number?
print(2 / number)  # What if the user inputs 0?

# To avoid zero-division errors you could:
try:
    print(2 / number)
except:
    print("oh, something went wrong")

# If an error happens within the try indent, the except indent is executed instead
# This is not best-pratice, since any error would casue the except statement to trigger
# Instead specify which error, like so:
try:
    print(2 / number)
except ZeroDivisionError:
    print("please don't choose 0")
except TypeError:
    print("please choose an integer or a float")

# You can be even more verbose by capturing the original error and display it
# alongside yours
try:
    print(2 / number)
except ZeroDivisionError as e:
    print(e)                        # => division by zero
    print("please don't choose 0")  # => please don't choose 0

# To make sure that the user doesn't break the code due to a bad input, you can
# combine what we know about loops and exceptions.
while True:
    try:
        integer = int(input("input integers: "))
        break
    except ValueError:
        print("only integers please")
        continue

print(integer)

# Alternatively when checking for the existance of a value, you could utilize None
# None is a way to declare a variable without assigning it an acctual value.
# Just like with truthness, null/none-ness should be checked using 'is'

num = None
while num is None:
    try:
        num = int(input("input integers: "))
    except ValueError:
        print("only integers please")
        continue

print(num)


###############################################################################
# 11. Functions
###############################################################################

# This guide has already showed multiple pre-made functions. In Python you can
# also make your own functions, with the folloing general syntax:
# def <name>():
#     <code-block>

def example():
    pass

# If you use a piece of code often in your script, you could bundle them in a function.


def getint():
    while True:
        try:
            var = int(input("input any number: "))
            break
        except ValueError:
            continue
    return var


###############################################################################
# 12. Python Tactics
###############################################################################

# The Python language is a tool, used to solve a problem.
# A problem doesn't have a single solution, there can be many ways of solving it.
# As such it's not necessary to know every single function or operand, moreover
# the internet has many great resources for looking up pieces of code.
# Python also has a builtin help function:
# help("")  # print the help text for strings, looks quite intimidating though
# for most coding you can ignore any functions formated like this: "__function__"


# To simplify problem solving employ different strategies

# Divide and conquer
# https://en.wikipedia.org/wiki/Problem_solving#Problem-solving_strategies
# https://en.wikipedia.org/wiki/Divide-and-conquer_algorithm
# Mind maps - what needs to be done
# Trial and Error combined with research (broad/specific)
# try to think logically, few problems can be solved purely mathematically

# Always try to write Pythonic code, or more simply write clear and
# understandable code, at least from your point of view.
# And when necessary use comments to explain your code
