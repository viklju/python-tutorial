#!/usr/bin/env python3
###############################################################################
# 13. Lists
###############################################################################

# There are even more types of data in Python

# Lists (commonly called arrays in other code-languages)
li = []          # an empty list
list1 = [1, 2, 3]  # a list containing three values, the integers 1, 2 and 3
type(li)         # => <class 'list'>

# Add to/remove from a list
li = []
li.append(2)     # => li = [2]
li.append("h")   # => li = [2, 'h']
li = [1, 2, 3, 4, 5]
li.pop()         # => li = [1, 2, 3, 4]
li.pop(0)        # => li = [2, 3, 4]
li.remove(2)     # => li = [3, 4]
li.insert(1, 2)  # => li = [3, 2, 4]
li.clear()       # => li = []

# Pop removes value with key n, which by default is -1 i.e. the last value
# Remove, removes the value passed to it

# Combining lis
list1 = [1, 2, 3]
list2 = ['a', 'b', 'c']
list1 + list2        # => [1, 2, 3, 'a', 'b', 'c']
list1 += [2]         # => list1 = [1, 2, 3, 2]
list1.extend(list2)  # => list1 = [1, 2, 3, 2, 'a', 'b', 'c']

# Index/key lists
li = [1, 2, 3, 4, 5, 6]
li[0]        # => 1
li[2] = 22   # => li = [1, 2, 22, 4, 5, 6]
li[-1]       # => 6
li[-4]       # => 22
li[5]        # => 6
li.index(4)  # => 3   return the index (3) of the value 4 in the list
# Lists begin at zero and end at -1

# Count occurences in a list
li = [1, 1, 3, 1, 4]
li.count(1)  # => 3

# Find in list
"hi" in ["hi", "hello", "greetings"]  # => True please note the argument order

# Return value from list, from key/index n to m
li = [1, 2, 3, 4, 5, 6]
li[1:3]  # => [2,3]
li[3:]   # => [4, 5, 6]
li[3:4]  # => [4]

# Return everyother value
li[::2]   # => [1, 3, 5]
# list[start:stop:step]

# Return the list in reverse
li[::-1]      # => [6, 5, 4, 3, 2, 1]
li.reverse()  # => li = [6, 5, 4, 3, 2, 1]

# Copy lists
li = [1, 2, 3, 4, 5, 6]
copy = li[:]      # => copy = [1, 2, 3, 4, 5, 6]
copy = li.copy()  # => copy = [1, 2, 3, 4, 5, 6]

# Sort lists
vowels = ['e', 'a', 'u', 'o', 'i']
vowels.sort()              # => vowels = ['a', 'e', 'i', 'o', 'u']
primes = [5, 13, 2, 19, 17, 11, 3, 7]
primes.sort(reverse=True)  # => primes = [19, 17, 13, 11, 7, 5, 3, 2]
sorted(primes)             # => [2, 3, 5, 7, 11, 13, 17, 19]
# The sorted function works just like the sort() method
li = ["a", "abcd", "hjk"]
li.sort(key=len)
# => li = ['a', 'hjk', 'abcd'] sort based on the lenght function


###############################################################################
# 14. More Indexing
###############################################################################

# You can index strings too
"hello world"[0]    # => 'h'
"hello world"[-3]   # => 'r'
"hello world"[::2]  # => 'hlowrd'

# Create lists from strings
"hello world".split(" ")  # => ['hello', 'world']
"hello world".split("l")  # => ['he', '', 'o wor', 'd']

"try splitting a string\nwith multiple lines\nit can be very useful".splitlines()
# => ['try splitting a string', 'with multiple lines', 'it can be very useful']

# Create strings from lists
", ".join(["spam", "parrot", "rabbit", "inquisition"])
# => 'spam, parrot, rabbit, inquisition'

"".join(["spam", "parrot", "rabbit", "inquisition"])
# => 'spamparrotrabbitinquisition'

"hi" in "hello! hi, how are you?"        # => True
"hi" in ["hello", "hi", "how are you?"]  # => True
2 in [1, [1, 2, 3], 3]                   # => False
3 in [1, [1, 2, 3], 3]                   # => True


###############################################################################
# 15. Lists and Functions
###############################################################################

# The length of a list
li = [1, 2, 3, 4, 5, 6]
len(li)  # => 6

# Get the smallest value
list1 = [1, 2, 3, 4, 5, 6]
min(list1)                   # => 6
list2 = [22, 3, 4, 55]
min(list1, list2)            # => [1, 2, 3, 4, 5, 6]
min(["bee", "wasp", "ant"])  # => 'ant'

# Get the largest value
list1 = [1, 2, 3, 4, 5, 6]
max(list1)                   # => 1
list2 = [22, 3, 4, 55]
max(list1, list2)            # => [22, 3, 4, 55]
max(["bee", "wasp", "ant"])  # => 'wasp'

# Sum lists
sum([1, 2, 3, 4])  # => 10

# Nesting lists
li = [1, 2, [11, 12, 13], 3, [21, 22]]
li[2]              # => [11, 12, 13]
li[2][1]           # => 12
li[3]              # => 3
len(li)            # => 5
len(li[2])         # => 3
li[0] = [1, 2, 3]  # => [[1, 2, 3], 2, [11, 12, 13], 3, [21, 22]]

# Create list from iterable (e.g. other lists or range-function)
list([2])                # => [2]
list([12, 435, 6])       # => [12, 435, 6]
list(range(4))           # => [0, 1, 2, 3]
# Make sure you don't have a list called list, overwritting the function list

# For-loops can iterate over iterables, i.e. you can loop over the values of a list
# like so:
li = ["bee", "wasp", "ant"]
for i in li:
    print(i)

# Note: you should not change a list your iterating over whitin a loop,
# this can cause unexpected consequence
# If you need to change the list whitin the loop, make a copy first.
li = ["bee", "wasp", "ant"]
# temp = li.copy()
temp = li
for i in li:
    temp.remove(i)

# Note: copying by; temp = li would cause temp to be a reference to li
# and any changes made to temp would also be made to li (and vice versa)

# Try iterating over a nested list, what happens?
# How would you iterate over all values in a nested list?

###############################################################################
# 16. Tuples
###############################################################################

# Immutable lists - Tuples
tupe = (1, 2, 3)
tupe[2]     # => 3
type(tupe)  # => <class 'tuple'>

# Tuples act just like lists, except once created they can't be changed
# you cannot add or remove from a tuple, e.g.
# tuple[2] = 2  # Raises a TypeError
tupe = ('a', 'b', [1, 2, 3], (22, 34))  # tuples can only be recreated
# Though lists whitin a tuple can still be changed
tupe[2].append(4)  # tupe = ('a', 'b', [1, 2, 3, 4], (22, 34))

# Tuples are prefered over lists when you want to highlight for another 
# programmer that a list will not be changed in your code, you can use a tuple
# instead.

(1, 2, 1, 1).count(1)  # => 3
min((1, 22, 11, 35))   # => 1
max((1, 22, 11, 35))   # => 35
len((1, 22, 11, 35))   # => 4
list(tupe)             # => ['a', 'b', [1, 2, 3, 4], (22, 34)]
tuple(range(4))        # => (0, 1, 2, 3)

# For-loops can also loop over tuples, since they too are iterables
li = ("fishy", "fishes", "fishing")
for i in li:
    print(i)

# Unpacking variables
# or assigning values, and save some typing part 2
1, 2, 3, 4               # => (1, 2, 3, 4)
[a, b, c] = (1, 2, 3)    # => a = 1, b = 2, c = 3
e, *f, g = (1, 2, 3, 4)  # => e = 1, f = [2, 3], g = 4
a, b = e, f              # => a = 1, b = [2, 3]
# this also works with lists


###############################################################################
# 17. Dictionaries
###############################################################################

# Dictoinaries are a way to link/mapp a name to a value(s)

d = {}  # => an empty dictionary
d = {'a': 'hello', 'b': 'world'}
# => a dictionary with the keys 'a' and 'b', and the values 'hello' and 'world'

'hello' in d  # => True
'b' in d      # => False

# Returning values from dictionaries
d['a']         # => 'hello' (keying out the value 'hello', with the key 'a')
d['a'] = "hi"  # d = {'a': 'hi', 'b': 'world'}
var = list(d.keys())    # var = ['a', 'b']
var = list(d.values())  # var = ['hello', 'world']
var = list(d.items())   # var = [('a', 'hi'), ('b', 'world')]

# Note that dictionary methods return an object, and to work with it first
# turn it into a list or tuple

d.pop('a')  # d = {'b': 'world'}
d.get('b')  # => 'world' equivalent to d['b']
d.clear()   # d = {}
d = {'1': 1, 2: '2'}.copy()  # d = {'1': 1, 2: '2'}
# Note: you can mix strings and integers as keys or values

# Dictionaries and functions
min({1: 'a', 22: 'b', 13: 'c'})   # => 1
max({1: 'a', 22: 'b', 13: 'c'})   # => 22
len({1: 'a', 22: 'b', 13: 'c'})   # => 3

dict(name='John', age=17)     # => {'name': 'John', 'age': 17}


###############################################################################
# 18. Structuring Data
###############################################################################

# Nested dictionaries
d = {1: [1, 2, 3], 2: (1, 2, 3)}
d[3] = list(d.values())
# d = {1: [1, 2, 3], 2: (1, 2, 3), 3: [[1, 2, 3], (1, 2, 3)]}

# Multiline dictionaries
d = {
    1: [1, 2, 3],
    2: (1, 2, 3)
}

# Multiline lists/tuples
li = [
    (1, 2, 3),
    (2, 3, 4),
    (3, 4, 5)
]

# this can be very useful when working with more complex data structures, e.g.
d = {
    1: [
        (1, 2, 3),
        (2, 3, 4),
        (3, 4, 5)
        ],
    2: (
        [1, 2, 3],
        [2, 3, 4],
        [3, 4, 5]
        )
}


###############################################################################
# 18. Printing Complex Data
###############################################################################

# Let's say we list, and we want to print it
example = [1, 2, 3, 4, 5]
print(example)  # => [1, 2, 3, 4, 5]
# What if we only want to see the values inside of the list?
print(*example)  # => 1 2 3 4 5
# Then you can unpack the list, and pass that to the print-function
# Let's say we want to print each value on a seperate row
print(*example, sep="\n")
# this and other print examples are covered in "python tutorial string magic"
# => 1
# => 2
# => 3
# => 4
# => 5
