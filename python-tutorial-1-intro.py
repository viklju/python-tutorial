#!/usr/bin/env python3
###############################################################################
# 0. Basics
###############################################################################

# Single line comments, or 'skipped code', starts with a hashtag

# Python versions are fairly compatible
# Still this guide is written for Python 3+, there will be inconsistencies with
# older versions

# Please try some of the code in a Python shell or from a file
# copy and paste, pick the code apart and see what each part does
# Experimenting is not only fun, but helps you learn
# If you get stuck; "Google is your friend" (other search engines are avilable)

# The string, or text: hello world
"hello world"

# The string, or text: hello world
'hello world'

# Multiline strings
"""
hello world
how are you?
"""

'''
hello world
how are you?
'''

# To show executed code, utlize the print-function, like so
print("hello world")
# Probably the most useful function when learning, most of what's desribed in
# this guide will give no ouput, use the print-function to show what's going on

# Though if you run code in a shell, Python is slightly more verbose


###############################################################################
# 1. Numbers
###############################################################################

# "Heltal" are called integers in Python
# Numbers with decimals are called floats, i.e. 3.2

# Any float or integer can be made into a string,
# but all strings cannot be made into float/integers


5         # => 5    (5 as an integer)
str(5)    # => '5'  (5 as a string)
int('5')  # => 5    (the string 5, made into an integer)
float(5)  # => 5.0  (an integer, made into a float)
int(5.6)  # => 5    (a float rounded down to an integer)

# Check type with type!
type(8)    # => <class 'int'>
type("8")  # => <class 'str'>

# The print-function can show strings, integers and floats
print(3)
print(12.4)

# But cannot mix strings and numbers, whitout more "advanced tricks"
# print("hello" 3)
# print(4 79)   won't work either since the space, ' ' is a string


###############################################################################
# 2. Arithmetic Operators
###############################################################################

# Basic math can be done using operators, integers and/or floats
1 + 1   # => 2
9 - 2   # => 7
3 * 4   # => 12
10 / 5  # => 2.0    note that division always returns a float

# Python isn't fuzzy about spacing, though don't forget readability
1+1       # => 2
9  -   2  # => 7
3    *4   # => 12
10/  5    # => 2.0

# Floor division, rounds down
5 // 3      # => 1
5.0 // 3.0  # => 1.0

# Modulo operation (remainder)
7 % 3  # => 1   3 - (7 // 3) => 3 - 2
8 % 2  # => 0   4 - 4

# Exponentiation
3 ** 2  # => 9  three to the power of two

# Parenthesis, can enforce precedence
(1 + 2) * 3    # => 9
1 + (2 * 3)    # => 7
1 + 2 * 3      # => 7   though operators are executed intelligently

# 2(3) is not the same as 2 * 3
# executing this would try to call the function 2, and pass it the integer 3
# similar to how the print-function was previouesly used:
print("hi, again!")  # calling the print-fucntion, and passing it a string

# Just like numbers, strings can be added too
"Hello" + "World"  # => "HelloWorld"

# Which can be used for easy/simple string and integer combination
"Hello, mr. " + str(3)  # => 'Hello, mr. 3'

# The operators can be placed in functions, like so:
print(12 / 33)
# Note: 12 / 33 is evaulated first before being sent to the print-function

###############################################################################
# 3. Variables
###############################################################################

# Variables are conventionaly lowercase alphanumerical descriptive names (PEP8)
# Though single letters, CamelCase, mixedCase and what_ever_this_is, is also often used
var1 = "hello world"
print(var1)
var2 = "How are you?"

# Operators can use variables instead
var1 + var2  # => "hello worldHow are you?"

3 * var2  # => "How are you?How are you?How are you?"

# Please note that most operators cannot mix strings and integers/floats
# for example "hi!" + 1, or "hi!" / 3 will result in an error
# e.g. var2 * 3  note the difference in position

var1 = 3
var2 = 5
var1 + var2  # => 8

# Variables can be overwritten
var1 = 3
var1 = var1 + 1  # => 4
var1 += 1        # => 5
var1 -= 4        # => 1

# Trying to use a variable before it has been assigned a value
# will result in an error, for example:
# var3 + var1

# Variables can be assigned based on other variables
var1 = 3
var2 = 5
var3 = var1 + var2  # => 8
var1 = 2
var3  # => 8 not 7


# But cannot not be written as freely as an equation, i.e.:
var1 = 3
var2 = 5
# var1 = var2 - var5  # => NameError: name 'var5' is not defined
# In this example var1 is overwritten by calculating var2 - var5
# and since var5 hasn't been assigned a value, var1 cannot be assigned a new value

###############################################################################
# 4. More Operators
###############################################################################

# Boolean values are essentially yes or no values (mind the capitalization)
True
False

type(False)  # => <class 'bool'>

# Negate evaluations with not
not True  # => False
not False  # => True

# Boolean operators
True and False  # => False  logical AND-gate
True or False   # => True   logical OR-gate

# Equality operators
var = 2
var == 0  # => False
2 == var  # => True

# Inequality operators
2 != 0  # => True

# Evaluate truthness
True is True       # => True
False is not True  # => True
# The recommended way to check for truthness, but avoid it when comparing values

True or True is True    # => True
True and False is True  # => False

# More comparison operators
1 < 10  # => True
1 > 10  # => False
2 <= 3  # => True
2 >= 2  # => True

# Checking whether a value is in a range
1 < 2 and 2 < 3  # => True
2 < 3 and 3 < 2  # => False
1 < 2 < 3        # => True

# Check type, using the isinstance-function
isinstance(2, str)    # => False
isinstance(2, int)    # => True
isinstance(2, float)  # => False
isinstance(2, bool)   # => False

# Find in string
"hello" in "hello world"  # => True
"22" in "221"             # => True


###############################################################################
# 5. Comparison Logic
###############################################################################

# Do x if y, else z (oneliners)
"hello!" if 3 > 2 else 2  # => "hello!"
"hello!" if 3 > 4 else 2  # => 2

# Often written as an if-else-statement (can be less restrictive to work with)
if 3 > 2:
    "hello!"
else:
    2

# Note: any control statement (e.g. if) must always ends with a colon, and an
# indented codeblock of atleast one line
# An indent is four blank spaces, or a single tab of equivalent lenght

# Everything whitin that indentation will be executed IF,
# and only if that statement is True.
# i.e. 3 > 2  => True, therefore print("hello!") is executed.
# The else-statement is only True if the previous if-statement is False.

# Single if
if True:
    print("True")

# Many ifs
if True:
    print("True")
if not False:
    print("not False")
if False:
    print("False")

# An if-statement doesn't need to be followed by an else-statment
# Each if-statemnet is evaluated independently

# If-comparison can be useful to check what a variable contains, like so
compare = "5"

if type(compare) is int:
    print("is integer")
if type(compare) is float:
    print("is float")
if type(compare) is str:
    print("is string")
if type(compare) is bool:
    print("is boolean")

# Note: all types are checked with 'is'

# The elif-statement
# When you want to compare multiple statments, like the one above (Many ifs),
# but only want to execute the first statement that evaluates to True.
# Instead of all that evaluate to True (i.e. dependent evaluation)

if "first statement" is True:
    print("execute only this (first)")
elif "first statement" is False and "second statement" is True:
    print("execute only this (second)")
elif "first statement" and "second statement" is False and "third statement" is True:
    print("execute only this (third)")
else:
    print("if no statement is true, execute this")

# Note: the elif-statement must be preceded by an if-statement


# Nesting - Combining statements, beyond the one line (Note: mind the indents)
# Statements within statements, indent block wihitin indent block
if 3 != 2:
    if 3 > 2:
        print("if and only if")

elif not False:
    print("else and if")
else:
    if True:
        print("else and if?")

# The pass-statement can be useful when testing code with control statements.
# pass can replace any codeblock, and simply means that the script continues.
# Which I find particularly useful when writing code containing ':' (colons),
# since you can continue on other parts and simply leave a placeholder. E.g.:

if 2 > 3:  # Note the if-statement is still evaluted.
    pass


###############################################################################
# 6. Some Useful Functions
###############################################################################

# Find the lenght of a string
len("hello world!")  # => 12

# Get the absolut value
abs(-12)  # => 12
abs(12)   # => 12

# Round floats
round(123.56)     # => 123
round(123.56, 1)  # => 123.6

# Get the smallest value
min(1, 2, -3)  # => -3

# Get the largest value
max(1, 2, -3)  # => 2

# More integers
int(15)           # => 15
hex(15)           # => '0xF'    hexadecimal: F
int('0xF', 16)    # => 15
bin(15)           # => '0b1111' binary: 1111
int('0b1111', 2)  # => 15
oct(8)            # => '0o10'   octal: 10
int('0o10', 8)    # => 8


###############################################################################
# 7. Combining What We Know
###############################################################################

# Example 1
var = -12.6936
output = "error" if abs(var) == 0 else round(var ** 2)
output *= len(str(output))
print(output)

# Example 2
var = -12.6936
var = abs(var)

if var == 0:
    output = "error"
else:
    temp = var ** 2
    output = round(temp)

string = str(output)
print(output * len(string))

# Is there any difference in the two examples? Which is more readable?
# What value is assigned to output? Change the value of var, what happens?
# Try assigning 0 to var (var = 0)
