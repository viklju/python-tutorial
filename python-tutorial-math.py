#!/usr/bin/env python3
###############################################################################
# M1. Random
###############################################################################

# Working with random numbers
import random  # use methods from the random module
# Typically modules are imported at the top of a Python file, since methods
# from a module cannot be used before the module itself has been imported.

random.randint(1, 100)             # random number from 1 too 100
random.choice(["Heads", "Tails"])  # randomly chooses from a list

seq = [1, 2, 3, 4, 5, 6]
random.shuffle(seq)
print(seq)
# Note: how the shuffle method only updates the list,
# it doesn't return anything. And can therefore not be used to assign values


# Using random numbers
from random import randint  # Note: you don't need to import the entire module
var = randint(0, 10)

if var % 2 == 0:
    print("The number is even")
else:
    print("The number is odd")

# You could create code which attempts to deduce the random number...
# Or could you check for even/odd in another way, i.e. not by dividing by 2?

###############################################################################
# M2. Math Module
###############################################################################

import math

math.pi
math.tau
math.e

x = 3.21
math.floor(x)  # rounds up a float to an integer
math.ceil(x)   # rounds down a float to an integer

math.sqrt(16)   # the square root of 16
math.pow(4, 2)  # 4 to the power of 2 (i.e.: 4 ** 2)

# And many, many more math methods.
# A lot of them are for more advanced math, and quite a few can already be done
# using Pythons builtin arithmetic operators (covered in the intro, 2. Arithmetic Operators)


###############################################################################
# M3. Graphs
###############################################################################

import turtle


def draw_graph(x, y):
    graph_size = [
                (0, y_size),   # +y
                (0, -y_size),  # -y
                (x_size, 0),   # +x
                (-x_size, 0)   # -x
                ]
    for cordinates in graph_size:
        pencil.speed(10)
        pencil.goto(cordinates)
        pencil.write(cordinates, font=("Arial", 12))
        pencil.goto(0, 0)


def linear_line(k, m, colour="red"):
    pencil.penup()
    pencil.color(colour)
    pencil.speed(1)

    for x in range(-x_size, x_size):
        y = k * x + m

        if y_size >= y >= -y_size:
            pencil.goto(x, y)
            pencil.pendown()
            pencil.speed(10)


y_size = 400
x_size = 600

screen = turtle.Screen()
screen.bgcolor("white")
pencil = turtle.Turtle()

draw_graph(x_size, y_size)
linear_line(2, 5, colour="orange")
linear_line(10, 10, colour="purple")
linear_line(-10, 5, colour="red")

screen.exitonclick()


###############################################################################
# 22. Visualizing Data
###############################################################################

# https://mode.com/blog/python-data-visualization-libraries
